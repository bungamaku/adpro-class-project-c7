# Food Marathon! Bot
[![pipeline status](https://gitlab.com/bungamaku/adpro-class-project-c7/badges/master/pipeline.svg)](https://gitlab.com/bungamaku/adpro-class-project-c7/commits/master)
[![coverage report](https://gitlab.com/bungamaku/adpro-class-project-c7/badges/master/coverage.svg)](https://gitlab.com/bungamaku/adpro-class-project-c7/commits/master)

### Advance Programming Class C - Group 7 

### Members :
- 1706022104 - [Bunga Amalia Kurniawati](https://gitlab.com/bungamaku/)
- 1406527545 - [Nur Hidayat](https://gitlab.com/dayatbflash)
- 1506737666 - [Ismail Shalih Abdul Kholiq](https://gitlab.com/ismailshalih_ak)
- 1706979461 - [Salsabila Hava Qabita](https://gitlab.com/salsahava)
- 1506722720 - [Dwi Nanda Susanto](https://gitlab.com/dwi.nanda09)

### Description
FoodMarathon adalah sebuah LINE Chatbot yang dapat memberikan detail informasi mengenai tempat makan yang User inginkan. FoodMarathon memiliki fitur spesial untuk User yang hobi makan dan ingin berwisata kuliner di sekitar dengan menyatakan rencana ingin makan apa saja dan di daerah mana, lalu FoodMarathon akan membuatkan urutan makan dengan jarak tempuh yang optimal. Kemudian User juga dapat mengetahui berbagai macam informasi mengenai restoran tersebut seperti menu harian, review makanan atau minuman, dan rata-rata biaya makan yang dikeluarkan orang-orang untuk makan di restoran tersebut. FoodMarathon menggunakan open API Zomato sehingga FoodMarathon dapat memberikan informasi-informasi tertentu mengenai banyak restoran.

### Features
1. Food Marathon yaitu memberikan saran untuk User dengan banyak keinginan makanan. User menyebutkan ingin makan apa saja, lalu FoodMarathon akan menentukan makan apa saja dan dimana saja dengan urutan makan dan jarak tempuh yang optimal. (Nur Hidayat)
2. Mencari lokasi restoran berdasarkan posisi di mana User berada, budget, dan jenis restoran. Sehingga User bisa mendapatkan saran restoran yang sesuai. (Dwi Nanda Susanto)
3. Mendapatkan informasi review, menu harian, dan rata-rata biaya untuk makan yang diperlukan dari sebuah restoran tertentu yang User inginkan. (Salsabila Hava Qabita)
4. Memberikan informasi mengenai daftar restoran dan makanan populer di kota User. (Bunga Amalia Kurniawati)
5. Menyimpan informasi mengenai restoran dan makanan favorit milik User. (Ismail Shalih Abdul Kholiq)
