package foodmarathon.zomatoservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import retrofit2.Retrofit;

@EnableDiscoveryClient
@SpringBootApplication
public class ZomatoServiceApplication {
	public static void main(String[] args) { SpringApplication.run(ZomatoServiceApplication.class, args); }

}
