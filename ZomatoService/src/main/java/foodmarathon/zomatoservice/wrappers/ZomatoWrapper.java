package foodmarathon.zomatoservice.wrappers;

import com.google.gson.GsonBuilder;
import foodmarathon.zomatoservice.apiMasters.APIInterface;
import foodmarathon.zomatoservice.apiMasters.responseModels.GeocodeResults;
import foodmarathon.zomatoservice.apiMasters.responseModels.SearchResults;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;

public class ZomatoWrapper {
    private final String BASE_URL = "https://developers.zomato.com/api/v2.1/";

    public ZomatoWrapper(){}

    public SearchResults getSearchResults(String q,
                                          int count,
                                          double lat,
                                          double lon,
                                          double radius) throws IOException {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIInterface api = retrofit.create(APIInterface.class);
        Call<SearchResults> resultCall = api.getSearchCall(q, count, lat, lon, radius, "real_distance", "asc");
        Response<SearchResults> execute = resultCall.execute();
	    return execute.body();
    }

	public GeocodeResults getGeocodeResults(double lat,
	                                        double lon) throws IOException {

		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(BASE_URL)
				.addConverterFactory(GsonConverterFactory.create())
				.build();

		APIInterface api = retrofit.create(APIInterface.class);
		Call<GeocodeResults> resultCall = api.getGeocodeCall(lat, lon);
		Response<GeocodeResults> execute = resultCall.execute();
		return execute.body();
	}
}
