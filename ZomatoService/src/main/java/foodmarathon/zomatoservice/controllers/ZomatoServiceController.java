package foodmarathon.zomatoservice.controllers;

import foodmarathon.zomatoservice.apiMasters.responseModels.Restaurant;
import foodmarathon.zomatoservice.apiMasters.responseModels.Restaurant_;
import foodmarathon.zomatoservice.wrappers.ZomatoWrapper;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@RestController
@EnableFeignClients
public class ZomatoServiceController {
    private final Logger LOGGER = Logger.getLogger(ZomatoServiceController.class.getName());

    @PostMapping("/restaurants")
    public List<Restaurant_> searchRestaurants(@RequestBody SearchRequest search) throws IOException {
        ZomatoWrapper zomatoWrapper = new ZomatoWrapper();
        List<Restaurant> restaurants = zomatoWrapper.getSearchResults(
                search.getQ(),
                search.getCount(),
                search.getLat(),
                search.getLon(),
                search.getRadius()
        ).getRestaurants();
	    return restaurants.stream()
			    .map(Restaurant::getRestaurant)
			    .collect(Collectors.toList());
    }

    @PostMapping("/nearby-restaurants")
	public List<Restaurant_> nearbyRestaurants(@RequestBody GeocodeRequest geocode) throws IOException {
	    ZomatoWrapper zomatoWrapper = new ZomatoWrapper();
	    List<Restaurant> nearbyRestaurants = zomatoWrapper.getGeocodeResults(
	    		geocode.getLat(),
			    geocode.getLon()
	    ).getNearbyRestaurants();
	    return nearbyRestaurants.stream()
			    .map(Restaurant::getRestaurant)
			    .collect(Collectors.toList())
			    .subList(0, 4 > nearbyRestaurants.size() ? nearbyRestaurants.size() : 4);
    }

    @PostMapping("/top-cuisines")
    public List<String> topCuisines(@RequestBody GeocodeRequest geocode) throws IOException {
    	ZomatoWrapper zomatoWrapper = new ZomatoWrapper();
    	return zomatoWrapper.getGeocodeResults(
    			geocode.getLat(),
			    geocode.getLon()
	    ).getPopularity().getTopCuisines();
    }
}

