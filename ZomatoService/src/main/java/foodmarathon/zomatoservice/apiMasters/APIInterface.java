package foodmarathon.zomatoservice.apiMasters;

import foodmarathon.zomatoservice.apiMasters.responseModels.GeocodeResults;
import foodmarathon.zomatoservice.apiMasters.responseModels.SearchResults;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface APIInterface {


    @Headers("user-key:38ecbce75e30c22a4ee8ad8bc3accba2")
    @GET("search")
    Call<SearchResults> getSearchCall(@Query("q") String q,
                                      @Query("count") int count,
                                      @Query("lat") double lat,
                                      @Query("lon") double lon,
                                      @Query("radius") double radius,
                                      @Query("sort") String sort,
                                      @Query("order") String order);

    @Headers("user-key:2dbbf53dc912d7d332410955940ee936")
	@GET("geocode")
	Call<GeocodeResults> getGeocodeCall(@Query("lat") double lat,
	                                    @Query("lon") double lon);
}
