package foodmarathon.zomatoservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import foodmarathon.zomatoservice.controllers.GeocodeRequest;
import foodmarathon.zomatoservice.controllers.SearchRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import foodmarathon.zomatoservice.controllers.ZomatoServiceController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ZomatoServiceController.class)
public class ZomatoServiceApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@Test
	public void restaurants() throws Exception {
		SearchRequest anObject = new SearchRequest();

		anObject.setQ("bakso");
		anObject.setLat(-6.2);
		anObject.setLon(106.8);
		anObject.setRadius(4000);
		anObject.setCount(1);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson=ow.writeValueAsString(anObject );


		mockMvc.perform(post("/restaurants")
				.contentType(APPLICATION_JSON_UTF8)
				.content(requestJson))
				.andExpect(status().is2xxSuccessful());
	}

	@Test
	public void nearbyRestaurants() throws Exception {
		GeocodeRequest anObject = new GeocodeRequest();

		anObject.setLat(-6.2);
		anObject.setLon(106.8);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson=ow.writeValueAsString(anObject);

		mockMvc.perform(post("/nearby-restaurants")
				.contentType(APPLICATION_JSON_UTF8)
				.content(requestJson))
				.andExpect(status().is2xxSuccessful());
	}

	@Test
	public void topCuisines() throws Exception {
		GeocodeRequest anObject = new GeocodeRequest();

		anObject.setLat(-6.2);
		anObject.setLon(106.8);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson=ow.writeValueAsString(anObject );

		mockMvc.perform(post("/top-cuisines")
				.contentType(APPLICATION_JSON_UTF8)
				.content(requestJson))
				.andExpect(status().is2xxSuccessful());
	}


	@Test
	public void searchRestaurants() throws Exception {
		SearchRequest anObject = new SearchRequest();

		anObject.setQ("pasta");
		anObject.setLat(-6.126132);
		anObject.setLon(106.836698);
		anObject.setRadius(50000);
		anObject.setCount(14);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson=ow.writeValueAsString(anObject );


		mockMvc.perform(post("/restaurants")
				.contentType(APPLICATION_JSON_UTF8)
				.content(requestJson))
				.andExpect(status().is2xxSuccessful());
	}

	@Test
	public void searchNearbyRestaurants() throws Exception {
		GeocodeRequest anObject = new GeocodeRequest();

		anObject.setLat(-6.126132);
		anObject.setLon(106.836698);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson=ow.writeValueAsString(anObject);

		mockMvc.perform(post("/nearby-restaurants")
				.contentType(APPLICATION_JSON_UTF8)
				.content(requestJson))
				.andExpect(status().is2xxSuccessful());
	}

	@Test
	public void searchTopCuisines() throws Exception {
		GeocodeRequest anObject = new GeocodeRequest();

		anObject.setLat(-6.126132);
		anObject.setLon(106.836698);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson=ow.writeValueAsString(anObject );

		mockMvc.perform(post("/top-cuisines")
				.contentType(APPLICATION_JSON_UTF8)
				.content(requestJson))
				.andExpect(status().is2xxSuccessful());
	}


	@Test
	public void searchRestaurantsByQuery() throws Exception {
		SearchRequest anObject = new SearchRequest();

		anObject.setQ("sate");
		anObject.setLat(-7.280093);
		anObject.setLon(112.783783);
		anObject.setRadius(20000);
		anObject.setCount(20);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson=ow.writeValueAsString(anObject );


		mockMvc.perform(post("/restaurants")
				.contentType(APPLICATION_JSON_UTF8)
				.content(requestJson))
				.andExpect(status().is2xxSuccessful());
	}

	@Test
	public void searchNearbyRestaurantsByLatLong() throws Exception {
		GeocodeRequest anObject = new GeocodeRequest();

		anObject.setLat(-7.280093);
		anObject.setLon(112.783783);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson=ow.writeValueAsString(anObject);

		mockMvc.perform(post("/nearby-restaurants")
				.contentType(APPLICATION_JSON_UTF8)
				.content(requestJson))
				.andExpect(status().is2xxSuccessful());
	}

	@Test
	public void searchTopCuisinesByLatLong() throws Exception {
		GeocodeRequest anObject = new GeocodeRequest();

		anObject.setLat(-7.280093);
		anObject.setLon(112.783783);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson=ow.writeValueAsString(anObject );

		mockMvc.perform(post("/top-cuisines")
				.contentType(APPLICATION_JSON_UTF8)
				.content(requestJson))
				.andExpect(status().is2xxSuccessful());
	}



	@Test
	public void pageNotFound() throws Exception {
		mockMvc.perform(post("/halo")).andExpect(status().is4xxClientError());
	}

	@Test
	public void wrongMethod() throws Exception {
		mockMvc.perform(get("/index")).andExpect(status().is4xxClientError());
	}

}