package marathonservice.foodmarathon;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import marathonservice.foodmarathon.controllers.FormController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = FormController.class)
public class MarathonFormTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void formPage() throws Exception {
		mockMvc.perform(get("/form")).andExpect(content().string(containsString("What do you want to eat today?")));
	}

	@Test
	public void pageNotFound() throws Exception {
		mockMvc.perform(get("/halo")).andExpect(status().is4xxClientError());
	}

	@Test
	public void wrongMethod() throws Exception {
		mockMvc.perform(post("/index")).andExpect(status().is4xxClientError());
	}

}