package marathonservice.foodmarathon.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SuggestFoodsQuery {
    private double latitude;
    private double longitude;
    private String[] keywords;

    public SuggestFoodsQuery(double latitude, double longitude, String[] keywords) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.keywords = keywords;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public String[] getKeywords() {
        return keywords;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setKeywords(String[] keywords) {
        this.keywords = keywords;
    }
}
