package marathonservice.foodmarathon.models;

public class SearchRequest {
    private String q;
    private int count;
    private double lat;
    private double lon;
    private double radius;

    public SearchRequest(String q, int count, double lat, double lon, double radius) {
        this.q = q;
        this.count = count;
        this.lat = lat;
        this.lon = lon;
        this.radius = radius;
    }

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}