package marathonservice.foodmarathon.clients;

import marathonservice.foodmarathon.models.Restaurant;
import marathonservice.foodmarathon.models.SearchRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient("ZomatoService")
public interface ZomatoServiceClient {
    @PostMapping("/restaurants")
    public List<Restaurant> searchRestaurants(@RequestBody SearchRequest search);
}
