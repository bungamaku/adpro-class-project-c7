package marathonservice.foodmarathon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class MarathonServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarathonServiceApplication.class, args);
    }
}