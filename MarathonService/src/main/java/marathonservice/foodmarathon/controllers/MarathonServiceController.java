package marathonservice.foodmarathon.controllers;

import marathonservice.foodmarathon.clients.ZomatoServiceClient;
import marathonservice.foodmarathon.models.Location;
import marathonservice.foodmarathon.models.Restaurant;
import marathonservice.foodmarathon.models.SearchRequest;
import marathonservice.foodmarathon.models.SuggestFoodsQuery;
import marathonservice.foodmarathon.routings.BruteForceRouting;
import marathonservice.foodmarathon.routings.ManhattanDistance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@RestController
@SpringBootApplication
@EnableFeignClients(basePackages = { "marathonservice.foodmarathon.clients" })
public class MarathonServiceController {
    private final Logger LOGGER = Logger.getLogger(MarathonServiceController.class.getName());

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    ZomatoServiceClient zomatoServiceClient;

    @PostMapping("/suggest-foods")
    public List<Restaurant> suggestFoods(@RequestBody SuggestFoodsQuery requestBody) {

        Location depot = new Location(requestBody.getLatitude(), requestBody.getLongitude());
        List<Restaurant> restaurants = new ArrayList<>();
        for (String keyword : Arrays.asList(requestBody.getKeywords())) {
            SearchRequest searchRequest = new SearchRequest(keyword.trim(),
                    1, depot.getLatitude(), depot.getLongitude(), 5000);
            List<Restaurant> result = zomatoServiceClient.searchRestaurants(searchRequest);
            Restaurant restaurant = result.get(0);
            restaurant.setKeyword(keyword.trim());
            restaurants.add(restaurant);
        }

        BruteForceRouting bruteForceRouting = new BruteForceRouting(depot, restaurants, new ManhattanDistance());
        bruteForceRouting.execute();
        LOGGER.info("OPTIMUM COST " + bruteForceRouting.optimumCost);
        return bruteForceRouting.getSolution();
    }
}
