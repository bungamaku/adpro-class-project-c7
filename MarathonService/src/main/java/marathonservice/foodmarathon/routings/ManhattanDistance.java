package marathonservice.foodmarathon.routings;

import marathonservice.foodmarathon.models.Location;

public class ManhattanDistance implements DistanceStrategy {
    @Override
    public double distance(Location from, Location to) {
        Location middlePoint = new Location(from.getLatitude(), to.getLongitude());

        double distance1 = harvesineDistance(from, middlePoint);
        double distance2 = harvesineDistance(middlePoint, to);

        return distance1 + distance2;
    }

    private double harvesineDistance(Location from, Location to) {
        double dlat = to.getLatitude() - from.getLatitude();
        double dlon = to.getLongitude() - from.getLongitude();
        double a = Math.pow(Math.sin(dlat / 2.0), 2)
                 + Math.cos(from.getLatitude())
                 * Math.cos(to.getLatitude())
                 * Math.pow(Math.sin(dlon / 2.0), 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double r = 6371;
        return c * r * 1000;
    }
}
