package marathonservice.foodmarathon.routings;

import marathonservice.foodmarathon.models.Restaurant;

import java.util.List;

public interface RoutingStrategy {
    void execute();
    List<Restaurant> getSolution();
}