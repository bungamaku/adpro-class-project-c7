package marathonservice.foodmarathon.routings;

import marathonservice.foodmarathon.models.Location;

public interface DistanceStrategy {
    double distance(Location from, Location to);
}
