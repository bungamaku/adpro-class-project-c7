package marathonservice.foodmarathon.routings;

import marathonservice.foodmarathon.models.Location;
import marathonservice.foodmarathon.models.Restaurant;

import java.util.*;
import java.util.logging.Logger;

public class BruteForceRouting implements RoutingStrategy {
    private final Logger LOGGER = Logger.getLogger(BruteForceRouting.class.getName());
    public Location depot;
    public List<Restaurant> restaurants;
    public DistanceStrategy distanceStrategy;
    public double optimumCost;
    public List<Restaurant> optimumSequence;

    public BruteForceRouting(Location depot,
                             List<Restaurant> restaurants,
                             DistanceStrategy distanceStrategy) {
        this.depot = depot;
        this.restaurants = restaurants;
        this.distanceStrategy = distanceStrategy;
        optimumSequence = new ArrayList<>(Collections.nCopies(restaurants.size(), null));
        optimumCost = Double.MAX_VALUE;
    }

    @Override
    public void execute() {
        List<Restaurant> restaurantSequence = new ArrayList<>();
        Set<Restaurant> visitedRestaurants = new HashSet<>();
        solve(depot, 0, restaurantSequence, visitedRestaurants);
    }

    @Override
    public List<Restaurant> getSolution() {
        return optimumSequence;
    }

    public void solve(Location currentLocation,
                      double currentCost,
                      List<Restaurant> restaurantSequence,
                      Set<Restaurant> visitedRestaurant) {
        if (restaurantSequence.size() == restaurants.size()) {
            LOGGER.info("HASIL BRUTE COST: " + currentCost);
            if (optimumCost > currentCost) {
                optimumCost = currentCost;
                Collections.copy(optimumSequence, restaurantSequence);
                LOGGER.info("RESTAURANT SEQUENCE:");
                for(Restaurant restaurant: restaurantSequence) {
                    LOGGER.info(restaurant.getName());
                }
            }
        } else {
            for (Restaurant restaurant : restaurants) {
                if (!visitedRestaurant.contains(restaurant)) {
                    visitedRestaurant.add(restaurant);
                    restaurantSequence.add(restaurant);
                    Location newLocation = restaurant.getLocation();
                    solve(newLocation, currentCost + distanceStrategy.distance(currentLocation, newLocation),
                            restaurantSequence, visitedRestaurant);
                    restaurantSequence.remove(restaurantSequence.size() - 1);
                    visitedRestaurant.remove(restaurant);
                }
            }
        }
    }
}
