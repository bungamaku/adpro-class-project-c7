let counter = 1;
let empty = 3;

$(document).ready(function() {
    liff.init(async () => {
        const profile = await liff.getProfile();
        $("#name-inner").text(profile.displayName);
    });

    getLocation();
    sendFood();

    $(".add-more").click(() => {
        if (counter < 4) {
            counter++;
            empty += 2;
            let html = $(".copy").html();
            $(".after-add-more").after(html);
        } else {
            $('#max-food').modal();
        }
    });

    $("body").on("click",".remove",function(){
        counter--;
        empty -= 2;
        $(this).parents(".control-group").remove();
    });
});

let lat, long;

const getLocation = () => {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else {
        $('#en-loc').modal();
    }
};

const showPosition = (position) => {
    lat = position.coords.latitude;
    long = position.coords.longitude;
};

const showError = (e) => {
    if (e.PERMISSION_DENIED) {
        $('#en-loc').modal();
    }
}

let filled = 0;

const sendFood = () => {
    $("#submit").click(() => {
        const forms = document.getElementById("form-mar");
        let temp = "/marathon " + lat + "," + long + ";";
        let i;
        for (i = 0; i < forms.length; i++) {
            if (forms.elements[i].value.length > 0) {
                empty--;
                filled++;
                temp += forms.elements[i].value + ";";
            }
        }
        temp = temp.slice(0, -1);
        if (empty <= 1+filled && lat !== undefined && long !== undefined) {
            try {
                liff.sendMessages([
                    {
                        type: 'text',
                        text: temp
                    }
                ]).catch(error => {
                    $('#err-sorry').modal();
                    console.log("Error sending message: " + error);
                    setTimeout(liff.closeWindow, 1500);
                });
                $('#food-sent').modal();
                setTimeout(liff.closeWindow, 1500);
            }
            catch (e) {
                $('#err-sorry').modal();
                console.log(e);
                setTimeout(liff.closeWindow, 1500);
            }
        } else if (lat === undefined || long === undefined) {
            $('#wait-loc').modal();
        } else {
            $('#fill-all').modal();
        }
    });
};