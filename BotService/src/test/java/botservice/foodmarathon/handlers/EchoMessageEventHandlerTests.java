package botservice.foodmarathon.handlers;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.event.source.UserSource;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.Instant;
import java.util.Collections;
import java.util.List;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class EchoMessageEventHandlerTests {
    private EchoMessageEventHandler echoMessageEventHandler;
    private UserSource userSource;
    private TextMessageContent acceptedMessageContent;
    private TextMessageContent rejectedMessageContent;
    private List<Message> expectedResponse;

    @Before
    public void init() {
        userSource = new UserSource("userId");
        echoMessageEventHandler = new EchoMessageEventHandler();
        acceptedMessageContent = new TextMessageContent("1", "/echo test 1 2 3");
        rejectedMessageContent = new TextMessageContent("1", "test 1 2 3");
        expectedResponse = Collections.singletonList(new TextMessage("test 1 2 3"));
    }

    @Test
    public void acceptValidMessageContent() {
        MessageEvent<TextMessageContent> messageEvent = new MessageEvent<>("token",
                userSource, acceptedMessageContent, Instant.now());
        assertTrue(echoMessageEventHandler.canHandleTextMessageEvent(messageEvent));
    }

    @Test
    public void rejectInvalidMessageContent() {
        MessageEvent<TextMessageContent> messageEvent = new MessageEvent<>("token",
                userSource, rejectedMessageContent, Instant.now());
        assertFalse(echoMessageEventHandler.canHandleTextMessageEvent(messageEvent));
    }

    @Test
    public void responseToValidMessageEvent() {
        MessageEvent<TextMessageContent> messageEvent = new MessageEvent<>("token",
                userSource, acceptedMessageContent, Instant.now());

        List<Message> response = echoMessageEventHandler.handleTextMessageEvent(messageEvent);
        assertEquals(response, expectedResponse);
    }
}
