package botservice.foodmarathon;

import botservice.foodmarathon.controllers.BotServiceController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;


import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.jayway.restassured.RestAssured;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.ws.Response;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = BotServiceApplication.class)
@ActiveProfiles("default")
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class BotServiceApplicationTests {
    @LocalServerPort
    int port;

    @Test
    public void getDataTest() {
        get("/api/tdd/responseData").then().assertThat().body("data", equalTo("responseData"));
    }

    @Before
    public void setBaseUri() {
        RestAssured.port = port;
        RestAssured.baseURI = "http://localhost";
    }

    @Test
    public void contextLoads() {
    }
}
