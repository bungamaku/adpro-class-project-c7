package botservice.foodmarathon.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class GeocodeController {

	@GetMapping("/geocode")
	public String geocode() {
		return "geocode";
	}

}
