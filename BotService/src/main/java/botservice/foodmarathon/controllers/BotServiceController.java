package botservice.foodmarathon.controllers;

import botservice.foodmarathon.clients.MarathonServiceClient;
import botservice.foodmarathon.clients.ZomatoServiceClient;
import botservice.foodmarathon.handlers.EchoMessageEventHandler;
import botservice.foodmarathon.handlers.GeocodeMessageEventHandler;
import botservice.foodmarathon.handlers.MarathonMessageEventHandler;
import botservice.foodmarathon.handlers.MessageHandlerManager;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.message.LocationMessageContent;
import com.linecorp.bot.model.message.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.logging.Logger;

@LineMessageHandler
@RestController
@SpringBootApplication
@EnableFeignClients(basePackages = { "botservice.foodmarathon.clients" })
public class BotServiceController {

    private static final Logger LOGGER = Logger.getLogger(BotServiceController.class.getName());
    private MessageHandlerManager messageHandlerManager;
    private LineMessagingClient lineMessagingClient;
    private MarathonServiceClient marathonServiceClient;
    private ZomatoServiceClient zomatoServiceClient;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    public BotServiceController(LineMessagingClient lineMessagingClient,
                                MarathonServiceClient marathonServiceClient,
                                ZomatoServiceClient zomatoServiceClient,
                                MessageHandlerManager messageHandlerManager) {
        this.lineMessagingClient = lineMessagingClient;
        this.marathonServiceClient = marathonServiceClient;
        this.zomatoServiceClient = zomatoServiceClient;
        this.messageHandlerManager = messageHandlerManager;
        setUpHandler();
    }

    @EventMapping
    public void handleTextMessageEvent(MessageEvent<TextMessageContent> event) {
        List<Message> messages = messageHandlerManager.handleTextMessageEvent(event);
        replyMessages(event, messages);
    }

    @EventMapping
    public void handleLocationMessageEvent(MessageEvent<LocationMessageContent> event) {
        List<Message> messages = messageHandlerManager.handleLocationMessageEvent(event);
        replyMessages(event, messages);
    }

    public void setUpHandler() {
        // ADD HANDLERS HERE
        messageHandlerManager.addMessageHandler(new EchoMessageEventHandler());
        messageHandlerManager.addMessageHandler(new MarathonMessageEventHandler(marathonServiceClient));
        messageHandlerManager.addMessageHandler(new GeocodeMessageEventHandler(zomatoServiceClient));
    }

    public void replyMessages(MessageEvent event, List<Message> messages) {
        ReplyMessage replyMessage = new ReplyMessage(event.getReplyToken(), messages);
        lineMessagingClient.replyMessage(replyMessage);
    }

}
