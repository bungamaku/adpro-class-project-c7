package botservice.foodmarathon.clients;

import botservice.foodmarathon.models.Restaurant;
import botservice.foodmarathon.models.SuggestFoodsQuery;
import feign.Param;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.QueryParam;
import java.util.List;

@FeignClient("MarathonService")
public interface MarathonServiceClient {
    @PostMapping("/suggest-foods")
    List<Restaurant> suggestFoods(@RequestBody SuggestFoodsQuery requestBody);
}
