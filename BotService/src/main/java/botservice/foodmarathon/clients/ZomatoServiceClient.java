package botservice.foodmarathon.clients;

import botservice.foodmarathon.models.GeocodeRequest;
import botservice.foodmarathon.models.Restaurant;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient("ZomatoService")
public interface ZomatoServiceClient {
	@PostMapping("/nearby-restaurants")
	List<Restaurant> nearbyRestaurants(@RequestBody GeocodeRequest geocode);

	@PostMapping("/top-cuisines")
	List<String> topCuisines(@RequestBody GeocodeRequest geocode);
}