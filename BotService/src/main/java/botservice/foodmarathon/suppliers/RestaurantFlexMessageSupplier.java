package botservice.foodmarathon.suppliers;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;
import com.linecorp.bot.model.message.flex.container.FlexContainer;

import java.util.List;
import java.util.function.Supplier;

public class RestaurantFlexMessageSupplier implements Supplier<FlexMessage> {
    private List<Bubble> restaurantBubbles;

    public RestaurantFlexMessageSupplier(List<Bubble> restaurantBubbles) {
        this.restaurantBubbles = restaurantBubbles;
    }

    @Override
    public FlexMessage get() {
        FlexContainer container = Carousel.builder()
                .contents(restaurantBubbles)
                .build();

        FlexMessage restaurantFlexMessage = FlexMessage.builder()
                .altText("Restaurants")
                .contents(container)
                .build();

        return restaurantFlexMessage;
    }
}
