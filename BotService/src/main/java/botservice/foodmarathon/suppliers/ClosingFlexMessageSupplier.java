package botservice.foodmarathon.suppliers;

import botservice.foodmarathon.models.Restaurant;
import com.linecorp.bot.model.action.URIAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class ClosingFlexMessageSupplier implements Supplier<FlexMessage> {
    private List<Restaurant> restaurants;

    public ClosingFlexMessageSupplier(List<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }

    public FlexMessage get() {
        final Button allDirectionsButton = Button.builder()
                .style(Button.ButtonStyle.LINK)
                .height(Button.ButtonHeight.SMALL)
                .action(new URIAction("Get All Directions", getAllDirectionsUrl()))
                .build();

        final Box closeMessageBox = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(allDirectionsButton))
                .build();

        final Bubble closeMessageBubble = Bubble.builder()
                .body(closeMessageBox)
                .build();

        final FlexMessage closeMessage = FlexMessage.builder()
                .altText("Get All Directions")
                .contents(closeMessageBubble)
                .build();

        return closeMessage;
    }

    private String getAllDirectionsUrl() {
        String url = "https://www.google.com/maps/dir//";
        for (Restaurant restaurant : restaurants) {
            url += String.format("%s/", restaurant.getLocation().toString());
        }

        return url;
    }
}
