package botservice.foodmarathon.suppliers;

import botservice.foodmarathon.models.Restaurant;
import com.linecorp.bot.model.action.URIAction;
import com.linecorp.bot.model.message.flex.component.*;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;

import java.util.function.Supplier;

import static java.util.Arrays.asList;

public class NearbyRestaurantBubbleSupplier implements Supplier<Bubble> {
	private Restaurant nearbyRestaurant;

	public NearbyRestaurantBubbleSupplier(Restaurant nearbyRestaurant) {
		this.nearbyRestaurant = nearbyRestaurant;
	}

	@Override
	public Bubble get() {
		final Image heroBlock =
				Image.builder()
						.url(nearbyRestaurant.getThumb())
						.size(Image.ImageSize.FULL_WIDTH)
						.aspectRatio(Image.ImageAspectRatio.R20TO13)
						.aspectMode(Image.ImageAspectMode.Cover)
						.build();

		final Box bodyBlock = createBodyBlock();
		final Box footerBlock = createFooterBlock();
		final Bubble bubble =
				Bubble.builder()
						.hero(heroBlock)
						.body(bodyBlock)
						.footer(footerBlock)
						.build();

		return bubble;

	}

	private Box createBodyBlock() {
		final Text title =
				Text.builder()
						.text(nearbyRestaurant.getName())
						.weight(Text.TextWeight.BOLD)
						.size(FlexFontSize.Md)
						.build();

		final Text address =
				Text.builder()
						.text(nearbyRestaurant.getLocation().getAddress())
						.weight(Text.TextWeight.REGULAR)
						.size(FlexFontSize.SM)
						.wrap(true)
						.build();

		final Text locality =
				Text.builder()
						.text(nearbyRestaurant.getLocation().getLocality())
						.weight(Text.TextWeight.REGULAR)
						.size(FlexFontSize.SM)
						.wrap(true)
						.build();

		final Text city =
				Text.builder()
						.text(nearbyRestaurant.getLocation().getCity())
						.weight(Text.TextWeight.REGULAR)
						.size(FlexFontSize.SM)
						.build();

		return Box.builder()
				.layout(FlexLayout.VERTICAL)
				.contents(asList(title, address))
				.build();
	}

	private Box createFooterBlock() {
		final Separator separator = Separator.builder().build();
		final Button directionAction =
				Button.builder()
						.style(Button.ButtonStyle.LINK)
						.height(Button.ButtonHeight.SMALL)
						.action(new URIAction("Get Direction", generateDirectionURI(nearbyRestaurant.getLocation().toString())))
						.build();

		final Button websiteAction =
				Button.builder()
						.style(Button.ButtonStyle.LINK)
						.height(Button.ButtonHeight.SMALL)
						.action(new URIAction("Visit Zomato Page", nearbyRestaurant.getUrl()))
						.build();

		return Box.builder()
				.layout(FlexLayout.VERTICAL)
				.contents(asList(separator, directionAction, separator, websiteAction))
				.build();
	}

	private String generateDirectionURI(String destination) {
		return String.format("https://www.google.com/maps/dir//%s/", destination);
	}
}
