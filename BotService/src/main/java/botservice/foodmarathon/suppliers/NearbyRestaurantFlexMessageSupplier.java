package botservice.foodmarathon.suppliers;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;
import com.linecorp.bot.model.message.flex.container.FlexContainer;

import java.util.List;
import java.util.function.Supplier;

public class NearbyRestaurantFlexMessageSupplier implements Supplier<FlexMessage> {
	private List<Bubble> nearbyRestaurantBubbles;

	public NearbyRestaurantFlexMessageSupplier(List<Bubble> nearbyRestaurantBubbles) {
		this.nearbyRestaurantBubbles = nearbyRestaurantBubbles;
	}

	@Override
	public FlexMessage get() {
		FlexContainer container = Carousel.builder()
				.contents(nearbyRestaurantBubbles)
				.build();

		return FlexMessage.builder()
				.altText("Nearby Restaurants")
				.contents(container)
				.build();
	}
}
