package botservice.foodmarathon.handlers;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.*;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

import java.util.Collections;
import java.util.List;

public class EchoMessageEventHandler extends BaseMessageEventHandler {

    @Override
    public List<Message> handleTextMessageEvent(MessageEvent<TextMessageContent> event) {
        String responseText = event.getMessage().getText().replaceFirst("/echo ", "");
        return Collections.singletonList(new TextMessage(responseText));
    }

    @Override
    public boolean canHandleTextMessageEvent(MessageEvent<TextMessageContent> event) {
        return event.getMessage().getText().startsWith("/echo ");
    }

    @Override
    public boolean canHandleAudioMessageEvent(MessageEvent<AudioMessageContent> event) {
        return false;
    }

    @Override
    public boolean canHandleImageMessageEvent(MessageEvent<ImageMessageContent> event) {
        return false;
    }

    @Override
    public boolean canHandleLocationMessageEvent(MessageEvent<LocationMessageContent> event) {
        return false;
    }

    @Override
    public boolean canHandleStickerMessageEvent(MessageEvent<StickerMessageContent> event) {
        return false;
    }
}
