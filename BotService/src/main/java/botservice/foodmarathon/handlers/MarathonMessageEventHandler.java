package botservice.foodmarathon.handlers;

import botservice.foodmarathon.clients.MarathonServiceClient;
import botservice.foodmarathon.models.Restaurant;
import botservice.foodmarathon.models.SuggestFoodsQuery;
import botservice.foodmarathon.suppliers.ClosingFlexMessageSupplier;
import botservice.foodmarathon.suppliers.RestaurantBubbleSupplier;
import botservice.foodmarathon.suppliers.RestaurantFlexMessageSupplier;
import com.linecorp.bot.model.action.URIAction;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.*;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;


@LineMessageHandler
public class MarathonMessageEventHandler extends BaseMessageEventHandler {
    private final Logger LOGGER = Logger.getLogger(MarathonMessageEventHandler.class.getName());
    private MarathonServiceClient marathonServiceClient;

    public MarathonMessageEventHandler(MarathonServiceClient marathonServiceClient) {
        this.marathonServiceClient = marathonServiceClient;
    }

    @Override
    public List<Message> handleTextMessageEvent(MessageEvent<TextMessageContent> event) {
        List<Message> response = new ArrayList<>();
        String queryString = event.getMessage().getText().replaceFirst("/marathon ", "");
        String[] tokens = queryString.split(";");

        double latitude = Double.valueOf(tokens[0].split(",")[0]);
        double longitude = Double.valueOf(tokens[0].split(",")[1]);
        String[] keywords = Arrays.copyOfRange(tokens, 1, tokens.length);
        SuggestFoodsQuery query = new SuggestFoodsQuery(latitude, longitude, keywords);
        List<Restaurant> restaurants = marathonServiceClient.suggestFoods(query);

        String openMessage = "Here is the list of restaurants with your keyword and the optimal order to visit:\n";

        List<Bubble> restaurantBubbles = new ArrayList<>();
        int restaurantCounter = (int) 'A';
        for (Restaurant restaurant : restaurants) {
            String restaurantName = String.format("%s (%s)", restaurant.getName(), restaurant.getKeyword());
            openMessage += String.format("%s. %s\n", (char) restaurantCounter++, restaurantName);
            RestaurantBubbleSupplier restaurantBubble = new RestaurantBubbleSupplier(restaurant);
            restaurantBubbles.add(restaurantBubble.get());
        }
        TextMessage openTextMessage = new TextMessage(openMessage);
        response.add(openTextMessage);

        FlexMessage message = new RestaurantFlexMessageSupplier(restaurantBubbles).get();
        response.add(message);

        FlexMessage closeMessage = new ClosingFlexMessageSupplier(restaurants).get();
        response.add(closeMessage);

        return response;
    }

    @Override
    public boolean canHandleTextMessageEvent(MessageEvent<TextMessageContent> event) {
        return event.getMessage().getText().startsWith("/marathon ");
    }

    @Override
    public boolean canHandleStickerMessageEvent(MessageEvent<StickerMessageContent> event) {
        return false;
    }

    @Override
    public boolean canHandleLocationMessageEvent(MessageEvent<LocationMessageContent> event) {
        return false;
    }

    @Override
    public boolean canHandleImageMessageEvent(MessageEvent<ImageMessageContent> event) {
        return false;
    }

    @Override
    public boolean canHandleAudioMessageEvent(MessageEvent<AudioMessageContent> event) {
        return false;
    }
}
