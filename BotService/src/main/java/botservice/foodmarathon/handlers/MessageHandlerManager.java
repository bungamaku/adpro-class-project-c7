package botservice.foodmarathon.handlers;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.*;
import com.linecorp.bot.model.message.Message;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

@Service
public class MessageHandlerManager {
    private final Logger LOGGER = Logger.getLogger(MessageHandlerManager.class.getName());
    private List<MessageHandler> messageHandlers;

    public MessageHandlerManager() {
        messageHandlers = new ArrayList<MessageHandler>();
    }

    public void addMessageHandler(MessageHandler messageHandler) {
        messageHandlers.add(messageHandler);
        LOGGER.info("added handler: " + messageHandler.getClass().getName());
    }

    public List<Message> handleTextMessageEvent(MessageEvent<TextMessageContent> event) {
        for (MessageHandler messageHandler : messageHandlers) {
            if (messageHandler.canHandleTextMessageEvent(event)) {
                return messageHandler.handleTextMessageEvent(event);
            }
        }
        return Collections.emptyList();
    }

    public List<Message> handleImageMessageEvent(MessageEvent<ImageMessageContent> event) {
        for (MessageHandler messageHandler : messageHandlers) {
            if (messageHandler.canHandleImageMessageEvent(event)) {
                return messageHandler.handleImageMessageEvent(event);
            }
        }
        return Collections.emptyList();
    }

    public List<Message> handleLocationMessageEvent(MessageEvent<LocationMessageContent> event) {
        for (MessageHandler messageHandler : messageHandlers) {
            if (messageHandler.canHandleLocationMessageEvent(event)) {
                return messageHandler.handleLocationMessageEvent(event);
            }
        }
        return Collections.emptyList();
    }

    public List<Message> handleAudioMessageEvent(MessageEvent<AudioMessageContent> event) {
        for (MessageHandler messageHandler : messageHandlers) {
            if (messageHandler.canHandleAudioMessageEvent(event)) {
                return messageHandler.handleAudioMessageEvent(event);
            }
        }
        return Collections.emptyList();
    }

    public List<Message> handleStickerMessageEvent(MessageEvent<StickerMessageContent> event) {
        for (MessageHandler messageHandler : messageHandlers) {
            if (messageHandler.canHandleStickerMessageEvent(event)) {
                return messageHandler.handleStickerMessageEvent(event);
            }
        }
        return Collections.emptyList();
    }

}
