package botservice.foodmarathon.handlers;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.*;
import com.linecorp.bot.model.message.Message;

import java.util.List;

public interface MessageHandler {
    List<Message> handleTextMessageEvent(MessageEvent<TextMessageContent> event);
    List<Message> handleImageMessageEvent(MessageEvent<ImageMessageContent> event);
    List<Message> handleAudioMessageEvent(MessageEvent<AudioMessageContent> event);
    List<Message> handleStickerMessageEvent(MessageEvent<StickerMessageContent> event);
    List<Message> handleLocationMessageEvent(MessageEvent<LocationMessageContent> event);
    boolean canHandleTextMessageEvent(MessageEvent<TextMessageContent> event);
    boolean canHandleImageMessageEvent(MessageEvent<ImageMessageContent> event);
    boolean canHandleAudioMessageEvent(MessageEvent<AudioMessageContent> event);
    boolean canHandleStickerMessageEvent(MessageEvent<StickerMessageContent> event);
    boolean canHandleLocationMessageEvent(MessageEvent<LocationMessageContent> event);
}
