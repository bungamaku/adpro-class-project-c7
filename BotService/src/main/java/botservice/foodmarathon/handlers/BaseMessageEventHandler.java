package botservice.foodmarathon.handlers;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.*;
import com.linecorp.bot.model.message.Message;

import java.util.Collections;
import java.util.List;

public abstract class BaseMessageEventHandler implements MessageHandler {
    @Override
    public List<Message> handleAudioMessageEvent(MessageEvent<AudioMessageContent> event) {
        return Collections.emptyList();
    }

    @Override
    public List<Message> handleImageMessageEvent(MessageEvent<ImageMessageContent> event) {
        return Collections.emptyList();
    }

    @Override
    public List<Message> handleStickerMessageEvent(MessageEvent<StickerMessageContent> event) {
        return Collections.emptyList();
    }

    @Override
    public List<Message> handleTextMessageEvent(MessageEvent<TextMessageContent> event) {
        return Collections.emptyList();
    }

    @Override
    public List<Message> handleLocationMessageEvent(MessageEvent<LocationMessageContent> event) {
        return Collections.emptyList();
    }
}
