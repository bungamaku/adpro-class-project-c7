package botservice.foodmarathon.handlers;

import botservice.foodmarathon.clients.ZomatoServiceClient;
import botservice.foodmarathon.models.GeocodeRequest;
import botservice.foodmarathon.models.Restaurant;
import botservice.foodmarathon.suppliers.NearbyRestaurantBubbleSupplier;
import botservice.foodmarathon.suppliers.NearbyRestaurantFlexMessageSupplier;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.*;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


public class GeocodeMessageEventHandler extends BaseMessageEventHandler {
	private final Logger LOGGER = Logger.getLogger(GeocodeMessageEventHandler.class.getName());
	private ZomatoServiceClient zomatoServiceClient;

	public GeocodeMessageEventHandler(ZomatoServiceClient zomatoServiceClient) {
		this.zomatoServiceClient = zomatoServiceClient;
	}

	@Override
	public List<Message> handleTextMessageEvent(MessageEvent<TextMessageContent> event) {
		String[] queryString = event.getMessage()
				.getText()
				.replaceFirst("/geocode ", "")
				.split(",");
		double latitude = Double.valueOf(queryString[0]);
		double longitude = Double.valueOf(queryString[1]);
		GeocodeRequest geocodeRequest = new GeocodeRequest(latitude, longitude);
		List<Restaurant> nearbyRestaurants = zomatoServiceClient.nearbyRestaurants(geocodeRequest);
		List<String> topCuisines = zomatoServiceClient.topCuisines(geocodeRequest);

		TextMessage textMessage = new TextMessage("Here are some of the top cuisines around you: "
				+ String.join(", ", topCuisines)
				+ "\n\nand here is the list of nearby restaurants:");

		List<Bubble> nearbyRestaurantBubbles = new ArrayList<>();
		for (Restaurant nearbyRestaurant : nearbyRestaurants) {
			NearbyRestaurantBubbleSupplier nearbyRestaurantBubble = new NearbyRestaurantBubbleSupplier(nearbyRestaurant);
			nearbyRestaurantBubbles.add(nearbyRestaurantBubble.get());
		}

		FlexMessage message = new NearbyRestaurantFlexMessageSupplier(nearbyRestaurantBubbles).get();

		List<Message> response = new ArrayList<>();
		response.add(textMessage);
		response.add(message);

		return response;
	}

	@Override
	public boolean canHandleTextMessageEvent(MessageEvent<TextMessageContent> event) {
		return event.getMessage().getText().startsWith("/geocode ");
	}

	@Override
	public boolean canHandleStickerMessageEvent(MessageEvent<StickerMessageContent> event) {
		return false;
	}

	@Override
	public boolean canHandleLocationMessageEvent(MessageEvent<LocationMessageContent> event) {
		return false;
	}

	@Override
	public boolean canHandleImageMessageEvent(MessageEvent<ImageMessageContent> event) {
		return false;
	}

	@Override
	public boolean canHandleAudioMessageEvent(MessageEvent<AudioMessageContent> event) {
		return false;
	}
}