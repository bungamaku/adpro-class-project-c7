let lat, long;

$(document).ready(function() {
    liff.init(async () => {
        const profile = await liff.getProfile();
        $("#name-inner").text(profile.displayName);
    });
    $("#send-button").click(() => {
        $("#load-over").css("height", "100%");
        getLocation();
    });
});

const getLocation = () => {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else {
        $('#en-loc').modal();
    }
};

const showPosition = (position) => {
    lat = position.coords.latitude;
    long = position.coords.longitude;
    console.log("show" + lat + long);
    sendGeocode();
};

const showError = (e) => {
    if (e.PERMISSION_DENIED) {
        $('#en-loc').modal();
    }
};

const sendGeocode = () => {
    console.log(lat + long);
    if (lat !== undefined && long !== undefined) {
        let temp = "/geocode " + lat + "," + long;
        console.log(temp);
        try {
            liff.sendMessages([
                {
                    type: 'text',
                    text: temp
                }
            ]).catch(error => {
                $('#err-sorry').modal();
                console.log("Error sending message: " + error);
                setTimeout(liff.closeWindow, 1500);
            });
            $('#geo-sent').modal();
            setTimeout(liff.closeWindow, 1500);
        }
        catch (e) {
            $('#err-sorry').modal();
            console.log(e);
            setTimeout(liff.closeWindow, 1500);
        }
    } else {
        $('#err-sorry').modal();
        setTimeout(liff.closeWindow, 1500);
    }
};